package cn.bestwu.umeng.push;

import cn.bestwu.umeng.push.android.AndroidBroadcast;
import cn.bestwu.umeng.push.android.AndroidNotification;
import cn.bestwu.umeng.push.spring.PushClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * spring 环境测试
 *
 * @author Peter Wu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
public class SpringWebTest {

	@Autowired
	private PushClient pushClient;
	@Autowired
	private PushProperties properties;

	@Test
	public void testPushProperties() throws Exception {
		System.err.println(properties);
		System.err.println(properties.getAndroid());
		System.err.println(properties.getIos());
		System.err.println(properties.getAppkey());
		System.err.println(properties.getAndroid().getAppkey());
		System.err.println(properties.getIos().getAppkey());
	}

	@Test
	public void testPush() throws Exception {
		AndroidBroadcast broadcast = new AndroidBroadcast();
		broadcast.setTicker("Android broadcast ticker");
		broadcast.setTitle("中文 title");
		broadcast.setText("Android broadcast text");
		broadcast.goAppAfterOpen();
		broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		broadcast.setExtraField("test", "helloworld");
		pushClient.send(broadcast);
	}
}
