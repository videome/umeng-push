package cn.bestwu.umeng.push.android;

public class AndroidFilecast extends AndroidNotification {
	public AndroidFilecast() throws Exception {
		this.setPredefinedKeyValue("type", "filecast");
	}

	public void setFileId(String fileId) throws Exception {
		setPredefinedKeyValue("file_id", fileId);
	}
}