package cn.bestwu.umeng.push.android;

public class AndroidUnicast extends AndroidNotification {
	public AndroidUnicast() throws Exception {
		this.setPredefinedKeyValue("type", "unicast");
	}

	public void setDeviceToken(String token) throws Exception {
		setPredefinedKeyValue("device_tokens", token);
	}

}