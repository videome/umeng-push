package cn.bestwu.umeng.push.android;

public class AndroidBroadcast extends AndroidNotification {
	public AndroidBroadcast() throws Exception {
		this.setPredefinedKeyValue("type", "broadcast");
	}
}
