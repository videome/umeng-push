package cn.bestwu.umeng.push;

import java.io.IOException;
import java.util.Properties;

/**
 * 推送异常
 *
 * @author Peter Wu
 */
public class PushException extends RuntimeException {
	private static final long serialVersionUID = -6058615767619112392L;

	private static Properties msgProperties;

	private Object result;

	static {
		try {
			msgProperties = new Properties();
			msgProperties.load(PushException.class.getResourceAsStream("/errorMessages.properties"));
		} catch (IOException e) {
			throw new RuntimeException("初始化errorMessages.properties出错", e);
		}
	}

	public PushException(String error_code) {
		super(msgProperties.getProperty(error_code));
	}

	public PushException(String error_code, Object result) {
		super(msgProperties.getProperty(error_code) + "\nresult: " + String.valueOf(result));
		this.result = result;
	}

	public Object getResult() {
		return result;
	}
}
