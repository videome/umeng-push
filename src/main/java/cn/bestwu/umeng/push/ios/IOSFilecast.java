package cn.bestwu.umeng.push.ios;

public class IOSFilecast extends IOSNotification {
	public IOSFilecast() throws Exception {
			this.setPredefinedKeyValue("type", "filecast");
	}
	
	public void setFileId(String fileId) throws Exception {
    	setPredefinedKeyValue("file_id", fileId);
    }
}
