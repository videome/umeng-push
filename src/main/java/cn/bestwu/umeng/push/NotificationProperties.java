package cn.bestwu.umeng.push;

public class NotificationProperties {
	private String appkey;
	private String appMasterSecret;
	private boolean productionMode = true;

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	public String getAppMasterSecret() {
		return appMasterSecret;
	}

	public void setAppMasterSecret(String appMasterSecret) {
		this.appMasterSecret = appMasterSecret;
	}

	public boolean isProductionMode() {
		return productionMode;
	}

	public void setProductionMode(boolean productionMode) {
		this.productionMode = productionMode;
	}

	//--------------------------------------------

	@Override public String toString() {
		return "Properties{" +
				"appkey='" + appkey + '\'' +
				", appMasterSecret='" + appMasterSecret + '\'' +
				", productionMode=" + productionMode +
				'}';
	}
}