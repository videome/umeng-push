package cn.bestwu.umeng.push;

import cn.bestwu.umeng.push.android.AndroidNotification;
import cn.bestwu.umeng.push.ios.IOSNotification;
import org.json.JSONObject;

import java.time.Instant;

/**
 * 推送客户端
 *
 * @author Peter Wu
 */
public abstract class AbstractPushClient {

	private NotificationProperties android;
	private NotificationProperties ios;

	// The host
	protected final String host = "http://msg.umeng.com";

	// The upload path
	protected final String uploadPath = "/upload";

	// The post path
	protected final String postPath = "/api/send";

	public AbstractPushClient(PushProperties properties) {
		android = properties.getAndroid();
		if (android == null) {
			android = properties;
		}

		ios = properties.getIos();
		if (ios == null) {
			ios = properties;
		}
	}

	/**
	 * 请求服务器API
	 *
	 * @param url     请求URL
	 * @param request 请求内容
	 * @return 结果
	 */
	protected abstract Result postForEntity(String url, String request);

	/**
	 * 推送消息
	 *
	 * @param notification 信息
	 */
	private void sendNotification(UmengNotification notification) {
		String timestamp = String.valueOf(Instant.now().getEpochSecond());
		notification.setPredefinedKeyValue("timestamp", timestamp);

		String url = host + postPath;
		String postBody = notification.getPostBody();
		String sign = MD5.md5Hex("POST" + url + postBody + notification.getAppMasterSecret());
		url = url + "?sign=" + sign;

		Result result = postForEntity(url, postBody);
		if (result.getStatus() != 200) {
			throw new PushException(result.getBody().getJSONObject("data").getString("error_code"), result.getBody());
		}
	}

	/**
	 * 推送安卓消息
	 *
	 * @param notification 信息
	 */
	public void send(AndroidNotification notification) {
		notification.setAppMasterSecret(android.getAppMasterSecret());
		notification.setPredefinedKeyValue("appkey", android.getAppkey());
		notification.setPredefinedKeyValue("production_mode", android.isProductionMode());
		sendNotification(notification);
	}

	/**
	 * 推送安卓消息
	 *
	 * @param notification 信息
	 */
	public void send(IOSNotification notification) {
		notification.setAppMasterSecret(ios.getAppMasterSecret());
		notification.setPredefinedKeyValue("appkey", ios.getAppkey());
		notification.setPredefinedKeyValue("production_mode", ios.isProductionMode());
		sendNotification(notification);
	}

	/**
	 * Upload file with device_tokens to Umeng
	 *
	 * @param appkey          appkey
	 * @param appMasterSecret appMasterSecret
	 * @param contents        device tokens use '\n' to split them if there are multiple tokens
	 * @return 返回 file_id
	 */
	private String uploadContents(String appkey, String appMasterSecret, String contents) {
		// Construct the json string
		JSONObject uploadJson = new JSONObject();
		uploadJson.put("appkey", appkey);
		String timestamp = String.valueOf(Instant.now().getEpochSecond());
		uploadJson.put("timestamp", timestamp);
		uploadJson.put("content", contents);
		// Construct the request
		String url = host + uploadPath;
		String postBody = uploadJson.toString();
		String sign = MD5.md5Hex("POST" + url + postBody + appMasterSecret);
		url = url + "?sign=" + sign;

		Result result = postForEntity(url, postBody);
		JSONObject body = result.getBody();
		String ret = body.getString("ret");
		if (!ret.equals("SUCCESS")) {
			throw new PushException(body.getJSONObject("data").getString("error_code"), result.getBody());
		}
		JSONObject data = body.getJSONObject("data");
		return data.getString("file_id");
	}

	/**
	 * Upload file with IOS device_tokens to Umeng
	 *
	 * @param contents contents device tokens use '\n' to split them if there are multiple tokens
	 * @return 返回 file_id
	 */
	public String uploadIOSContents(String contents) {
		return uploadContents(ios.getAppkey(), ios.getAppMasterSecret(), contents);
	}

	/**
	 * Upload file with Android device_tokens to Umeng
	 *
	 * @param contents contents device tokens use '\n' to split them if there are multiple tokens
	 * @return 返回 file_id
	 */
	public String uploadAndroidContents(String contents) {
		return uploadContents(android.getAppkey(), android.getAppMasterSecret(), contents);
	}

}
