package cn.bestwu.umeng.push;

import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * HttpURLConnection 实现的默认推送客户端
 *
 * @author Peter Wu
 */
public class DefaultPushClient extends AbstractPushClient {

	public DefaultPushClient(PushProperties properties) {
		super(properties);
	}

	@Override protected Result postForEntity(String urlvalue, String request) {
		try {
			URL url = new URL(urlvalue);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);

			DataOutputStream out = new DataOutputStream(connection.getOutputStream());
			out.writeBytes(request);
			out.flush();
			out.close();

			InputStream inputStream;
			try {
				inputStream = connection.getInputStream();
			} catch (IOException e) {
				inputStream = connection.getErrorStream();
			}
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String inputLine = in.readLine();
			in.close();
			return new Result(connection.getResponseCode(), new JSONObject(inputLine));
		} catch (IOException e) {
			throw new RuntimeException("调用API失败", e);
		}
	}
}
