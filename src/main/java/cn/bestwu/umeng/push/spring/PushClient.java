package cn.bestwu.umeng.push.spring;

import cn.bestwu.umeng.push.AbstractPushClient;
import cn.bestwu.umeng.push.PushProperties;
import cn.bestwu.umeng.push.Result;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * spring RestTemplate 实现推送客户端
 *
 * @author Peter Wu
 */
public class PushClient extends AbstractPushClient {

	protected final RestTemplate restTemplate;

	public PushClient(PushProperties properties, RestTemplate restTemplate) {
		super(properties);
		this.restTemplate = restTemplate;
	}

	@Override protected Result postForEntity(String url, String request) {
		ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
		JSONObject body = new JSONObject(entity.getBody());
		return new Result(entity.getStatusCode().value(), body);
	}
}
