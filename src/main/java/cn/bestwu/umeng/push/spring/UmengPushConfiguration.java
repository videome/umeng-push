package cn.bestwu.umeng.push.spring;

import cn.bestwu.umeng.push.PushProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.feed.AtomFeedHttpMessageConverter;
import org.springframework.http.converter.feed.RssChannelHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.util.ClassUtils;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * spring 自动配置
 *
 * @author Peter Wu
 */
@ConditionalOnClass(RestTemplate.class)
@ConditionalOnMissingBean(PushClient.class)
@EnableConfigurationProperties(SpringPushProperties.class)
@Configuration
public class UmengPushConfiguration {

	@Autowired
	private PushProperties properties;
	@Resource
	private RestTemplate restTemplate;

	@Bean
	@Lazy
	public PushClient pushClient() {
		return new PushClient(properties, restTemplate);
	}

	@Bean
	@ConditionalOnMissingBean(name = "restTemplate")
	public RestTemplate restTemplate() {
		boolean romePresent =
				ClassUtils.isPresent("com.rometools.rome.feed.WireFeed", RestTemplate.class.getClassLoader());

		final boolean jaxb2Present =
				ClassUtils.isPresent("javax.xml.bind.Binder", RestTemplate.class.getClassLoader());

		final boolean jackson2Present =
				ClassUtils.isPresent("com.fasterxml.jackson.databind.ObjectMapper", RestTemplate.class.getClassLoader()) &&
						ClassUtils.isPresent("com.fasterxml.jackson.core.JsonGenerator", RestTemplate.class.getClassLoader());

		final boolean jackson2XmlPresent =
				ClassUtils.isPresent("com.fasterxml.jackson.dataformat.xml.XmlMapper", RestTemplate.class.getClassLoader());

		final boolean gsonPresent =
				ClassUtils.isPresent("com.google.gson.Gson", RestTemplate.class.getClassLoader());

		final List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new ByteArrayHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));//UTF-8 编码
		messageConverters.add(new ResourceHttpMessageConverter());
		messageConverters.add(new SourceHttpMessageConverter<>());
		messageConverters.add(new AllEncompassingFormHttpMessageConverter());

		if (romePresent) {
			messageConverters.add(new AtomFeedHttpMessageConverter());
			messageConverters.add(new RssChannelHttpMessageConverter());
		}

		if (jackson2XmlPresent) {
			messageConverters.add(new MappingJackson2XmlHttpMessageConverter());
		} else if (jaxb2Present) {
			messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
		}

		if (jackson2Present) {
			messageConverters.add(new MappingJackson2HttpMessageConverter());
		} else if (gsonPresent) {
			messageConverters.add(new GsonHttpMessageConverter());
		}
		RestTemplate restTemplate = new RestTemplate(messageConverters);
		restTemplate.setErrorHandler(new ResponseErrorHandler() {
			@Override public boolean hasError(ClientHttpResponse response) {
				return false;
			}

			@Override public void handleError(ClientHttpResponse response) {

			}
		});
		return restTemplate;
	}

}
