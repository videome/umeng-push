package cn.bestwu.umeng.push.spring;

import cn.bestwu.umeng.push.PushProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "umeng.push")
public class SpringPushProperties extends PushProperties {

}