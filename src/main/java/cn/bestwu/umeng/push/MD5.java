package cn.bestwu.umeng.push;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {

	private static final char[] HEX_CHARS =
			{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	private static char[] encodeHex(byte[] bytes) {
		char chars[] = new char[32];
		for (int i = 0; i < chars.length; i = i + 2) {
			byte b = bytes[i / 2];
			chars[i] = HEX_CHARS[(b >>> 0x4) & 0xf];
			chars[i + 1] = HEX_CHARS[b & 0xf];
		}
		return chars;
	}

	/**
	 * MD5加密
	 *
	 * @param data 待加密的字符串
	 * @return 加密后的内容
	 */
	public static String md5Hex(String data) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("Could not find MessageDigest with algorithm \"MD5\"", e);
		}
		return new String(encodeHex(md.digest(data.getBytes(Charset.forName("UTF-8")))));
	}

}