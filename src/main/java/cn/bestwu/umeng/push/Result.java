package cn.bestwu.umeng.push;

import org.json.JSONObject;

/**
 * API请求结果
 *
 * @author Peter Wu
 */
public class Result {

	private int status;

	private JSONObject body;

	public Result(int status, JSONObject body) {
		this.status = status;
		this.body = body;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public JSONObject getBody() {
		return body;
	}

	public void setBody(JSONObject body) {
		this.body = body;
	}
}
