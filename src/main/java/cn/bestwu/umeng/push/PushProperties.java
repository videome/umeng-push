package cn.bestwu.umeng.push;

/**
 * 推送配置属性
 *
 * @author Peter Wu
 */
public class PushProperties extends NotificationProperties {
	private NotificationProperties android;

	private NotificationProperties ios;

	public NotificationProperties getAndroid() {
		return android;
	}

	public void setAndroid(NotificationProperties android) {
		this.android = android;
	}

	public NotificationProperties getIos() {
		return ios;
	}

	public void setIos(NotificationProperties ios) {
		this.ios = ios;
	}

	//--------------------------------------------

	@Override public String toString() {
		return "PushProperties{" +
				"android=" + android +
				", ios=" + ios +
				"} " + super.toString();
	}

}