友盟推送JAVA服务端SDK

#### spring web 框架中用法
##### 引入SDK

* gradle
```gradle
compile 'cn.bestwu:umeng-push:1.5.5'
compile 'org.springframework.boot:spring-boot-starter-web:1.3.2.RELEASE'
```
* maven
```maven
<dependency>
    <groupId>cn.bestwu</groupId>
    <artifactId>umeng-push</artifactId>
    <version>1.5.5</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <version>1.3.2.RELEASE</version>
</dependency>
```

##### 配置账号信息
* properties配置：
```properties
# 统一信息（可选）
umeng.push.appkey: 123
umeng.push.appMasterSecret: 333
umeng.push.productionMode: false
# android 信息（可选）
umeng.push.android.appkey: 234
umeng.push.android.appMasterSecret: 4444
umeng.push.android.productionMode: true
# ios 信息（可选）
umeng.push.ios.appkey: 345
umeng.push.ios.appMasterSecret: 555
umeng.push.ios.productionMode: false
```
* 或yml配置：
```yml
umeng.push: # （可选）
  appkey: 123
  appMasterSecret: 333
  productionMode: false
  android:  # （可选）
    appkey: 234
    appMasterSecret: 4444
    productionMode: true
  ios: # （可选）
    appkey: 345
    appMasterSecret: 555
    productionMode: false
```
配置信息优先级为，先寻找android/ios配置信息，如果没有则使用统一信息（umeng.push.appkey等）

##### 使用例子
```java
import cn.bestwu.umeng.push.android.AndroidBroadcast;
import cn.bestwu.umeng.push.ios.IOSBroadcast;
import cn.bestwu.umeng.push.spring.PushClient;
import cn.bestwu.umeng.push.spring.UmengPushConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

@Component
@Import(UmengPushConfiguration.class)
public class Push {

	@Autowired
	private PushClient pushClient;

	public void sendAndroidBroadcast() throws Exception {
		AndroidBroadcast broadcast = new AndroidBroadcast();

		broadcast.setPredefinedKeyValue("description", "test");
		broadcast.setPredefinedKeyValue("ticker", "Android broadcast ticker");
		broadcast.setPredefinedKeyValue("title", "test push");
		broadcast.setPredefinedKeyValue("text", "helloworld");
		broadcast.setPredefinedKeyValue("after_open", "go_app");
		broadcast.setPredefinedKeyValue("display_type", "notification");
		pushClient.send(broadcast);
	}

	public void sendIOSBroadcast() throws Exception {
		IOSBroadcast broadcast = new IOSBroadcast();

		broadcast.setPredefinedKeyValue("badge", 1);
		broadcast.setPredefinedKeyValue("sound", "default");
		broadcast.setPredefinedKeyValue("description", "test");
		broadcast.setPredefinedKeyValue("alert", "helloworld");
		pushClient.send(broadcast);
	}
}
```
#### 非 spring web 框架中用法
##### 引入SDK

* gradle
```gradle
compile 'cn.bestwu:umeng-push:1.5.5'
```
* maven
```maven
<dependency>
    <groupId>cn.bestwu</groupId>
    <artifactId>umeng-push</artifactId>
    <version>1.5.5</version>
</dependency>
```

##### 使用例子
```java
import cn.bestwu.umeng.push.AbstractPushClient;
import cn.bestwu.umeng.push.DefaultPushClient;
import cn.bestwu.umeng.push.NotificationProperties;
import cn.bestwu.umeng.push.PushProperties;
import cn.bestwu.umeng.push.android.AndroidBroadcast;
import cn.bestwu.umeng.push.ios.IOSBroadcast;

public class Push {

	private AbstractPushClient pushClient;

	public Push() {
		PushProperties properties = new PushProperties();
		//设置android properties
		NotificationProperties android = new NotificationProperties();
		android.setAppkey("your android appkey");
		android.setAppMasterSecret("your android appMasterSecret");
		android.setProductionMode(false);
		properties.setAndroid(android);

		//ios properties
		NotificationProperties ios = new NotificationProperties();
		ios.setAppkey("your ios appkey");
		ios.setAppMasterSecret("your ios appMasterSecret");
		ios.setProductionMode(false);
		properties.setIos(ios);

		pushClient = new DefaultPushClient(properties);
	}

	public void sendAndroidBroadcast() throws Exception {
		AndroidBroadcast broadcast = new AndroidBroadcast();

		broadcast.setPredefinedKeyValue("description", "test");
		broadcast.setPredefinedKeyValue("ticker", "Android broadcast ticker");
		broadcast.setPredefinedKeyValue("title", "test push");
		broadcast.setPredefinedKeyValue("text", "helloworld");
		broadcast.setPredefinedKeyValue("after_open", "go_app");
		broadcast.setPredefinedKeyValue("display_type", "notification");
		pushClient.send(broadcast);
	}

	public void sendIOSBroadcast() throws Exception {
		IOSBroadcast broadcast = new IOSBroadcast();

		broadcast.setPredefinedKeyValue("badge", 1);
		broadcast.setPredefinedKeyValue("sound", "default");
		broadcast.setPredefinedKeyValue("description", "test");
		broadcast.setPredefinedKeyValue("alert", "helloworld");
		pushClient.send(broadcast);
	}
}
```